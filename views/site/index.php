<?php

use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="body-content">

        <p>
            «Добрый день, <b><?php echo Yii::$app->user->identity->username; ?>


            <?php
            echo Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Logout ',
                ['class' => 'btn btn-primary']
            )
            . Html::endForm()
            ?>
        </p>

    </div>

</div>
