<?php

namespace app\models;

use yii\helpers\ArrayHelper;

class TxtModel extends \yii\base\Object
{

    private static function getFileName()
    {
        return \Yii::getAlias('@data') . '/db.txt';
    }

    public static function getUsers()
    {

        $csvData = array_map('str_getcsv', file(self::getFileName()));

        $res = [];
        foreach ($csvData as $item) {

            $res[$item[0]] = [
                'id' => $item[0] ?? NULL,
                'username' => $item[1] ?? NULL,
                'password' => $item[2] ?? NULL,
                'authKey' => $item[3] ?? NULL,
                'accessToken' => $item[4] ?? NULL,
                'attempt' => $item[5] ?? NULL,
                'loginAt' => $item[6] ?? NULL,
            ];
        }
        return $res;
    }

    public static function saveUser($newUser)
    {
        $newUser = ArrayHelper::toArray($newUser);

        if(isset($newUser['id'])){

            $users = self::getUsers();
            $users[$newUser['id']] = $newUser;

            $res = '';
            foreach($users as $item){
                $res .= implode(',', $item);
                $res .= "\r\n";
            }

            file_put_contents (self::getFileName(), $res );
        }
    }
}
