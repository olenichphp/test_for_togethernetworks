<?php

namespace app\models;

class User extends TxtModel  implements \yii\web\IdentityInterface
{
    public $id;
    public $username;
    public $password;
    public $authKey;
    public $accessToken;
    public $attempt;
    public $loginAt;

    const MAX_ATTEMPT = 3;
    const MAX_BAN_TIME = 300; // 5min

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        $users = self::getUsers();
        return isset($users[$id]) ? new static($users[$id]) : null;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::getUsers() as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        foreach (self::getUsers() as $user) {
            if (strcasecmp($user['username'], $username) === 0) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }

    /**
     * @inheritdoc
     */
    public static function wrongLogin($userName){
        $user = self::findByUsername($userName);
        if(!is_null($user)){
            $user->attempt = ($user->attempt < self::MAX_ATTEMPT) ? ++$user->attempt : 1;
            $user->loginAt = time();
            self::saveUser($user);
        }
    }

    /**
     * @inheritdoc
     */
    public static function isBanAttempt($userName)
    {
        $user = User::findByUsername($userName);

        if(!is_null($user) && $user->attempt >= User::MAX_ATTEMPT){
            $timeDiff = time() - $user->loginAt;
            if($timeDiff <= self::MAX_BAN_TIME){
                return self::MAX_BAN_TIME - $timeDiff;
            }
        }
        return false;
    }



}
